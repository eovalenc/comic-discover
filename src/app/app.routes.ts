import { Routes } from '@angular/router'

import { RegistroComponent } from './components/registro/registro.component'
import { PersonajesComponent } from './components/personajes/personajes.component'

export const ROUTES: Routes = [
  { path: 'registro', component: RegistroComponent},
  { path: 'personajes', component: PersonajesComponent},
  { path: '', pathMatch: 'full', redirectTo: 'registro' },
  { path: '**', pathMatch: 'full', redirectTo: 'registro' },
]
