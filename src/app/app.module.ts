import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { RouterModule } from '@angular/router'
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component'
import { RegistroComponent } from './components/registro/registro.component'
import { PersonajesComponent } from './components/personajes/personajes.component'

import { ROUTES } from './app.routes'

import { CapitalizePipe } from './components/personajes/capitalize.pipe'

@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    PersonajesComponent,
    CapitalizePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES, { useHash: true })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
