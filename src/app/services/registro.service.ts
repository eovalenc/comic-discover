import { Injectable } from '@angular/core'
import { Registro } from '../components/registro/registro'

@Injectable({
  providedIn: 'root'
})
export class RegistroService {
  private registro: Registro

  constructor() {
    console.log(this.registro)
  }

  setRegistro(registro:Registro) {
    this.registro = registro
  }

  getRegistro(): Registro {
    return this.registro
  }
}


