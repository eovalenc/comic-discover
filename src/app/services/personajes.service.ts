import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class PersonajesService {
  constructor(private http: HttpClient) {
    console.log("Servicio")
  }

  getCharacters() {
    return this.http
      .get('http://localhost:3000/v1/characters')
  }

  setAuditCharacters(body: any) {
    return this.http
      .post('http://localhost:3000/v1/characters/audit', body)
  }
}
