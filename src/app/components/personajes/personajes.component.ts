import { Component } from '@angular/core'
import { Router } from '@angular/router'

import { PersonajesService } from '../../services/personajes.service'
import { RegistroService } from '../../services/registro.service'

@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html',
  styleUrls: ['./personajes.component.css']
})
export class PersonajesComponent {
  characters:any[] = []
  constructor(private route:Router, private service: PersonajesService, private _registroService: RegistroService) {
    if (!this._registroService.getRegistro()) {
      this.route.navigate(['/registro'])
      return;
    }

    this.service
      .getCharacters()
      .subscribe((res:any) => {
        console.log(res)
        this.characters = res.data.results
      })
  }

  handleClickUrls(sName: string, sUrl: string) {
    console.log(sName, sUrl)
    const user = this._registroService.getRegistro()
    console.log(user)
    this.service
      .setAuditCharacters({
        user,
        character: {
          name: sName,
          url: sUrl
        }
      })
      .subscribe((res) => {
        console.log(res)
      })
  }

}
