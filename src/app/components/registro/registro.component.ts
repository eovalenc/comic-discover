import { Component } from '@angular/core'
import { NgForm } from '@angular/forms'
import { Router } from '@angular/router'

import { Registro } from "./registro"
import { RegistroService } from '../../services/registro.service'

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
})
export class RegistroComponent {

  model = new Registro(null, null);

  constructor( private route:Router, private service:RegistroService) { }
  submitted = false;

  onSubmit(registro:NgForm) {
    this.submitted = true;
    console.log("Submit")
    console.log(registro)
    console.log("Value ", registro.value)
    console.log("Usuario", this.model)
    this.service.setRegistro(this.model)
    this.route.navigate(['/personajes'])
  }
  get diagnostic() { return JSON.stringify(this.model); }
}
