'use strict'

const express = require('express')
const asyncify = require('express-asyncify')

const marvelController = require('./marvel.controller')

const marvelRouter = asyncify(express.Router())

marvelRouter.get('/characters', marvelController.getCharacters)
marvelRouter.post('/characters/audit', marvelController.auditClickCharacters)

module.exports = marvelRouter
