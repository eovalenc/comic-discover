'use strict'

const Audit = require('../models/audit')

function createCharacterClickAudit (data) {
  return Audit.create(data)
}

module.exports = {
  createCharacterClickAudit
}
