'use strict'

const debug = require('debug')('comic-discover:components.marvel.controller')
const HTTPErrors = require('http-custom-errors')
const { get } = require('../utils/petitions')
const { createCharacterClickAudit } = require('./marver.service')

async function getCharacters (req, res, next) {
  debug('[GET] A request has come to /characters')

  let result
  try {
    let { data } = await get('/characters', '&comics=1499&orderBy=-name&limit=6')
    result = data
  } catch (error) {
    return next(error)
  }
  res.send(result)
}

async function auditClickCharacters (req, res, next) {
  debug('[POST] A request has come to /auditClickCharacters')

  if (Object.getOwnPropertyNames(req.body).length === 0) {
    return next(new HTTPErrors.BadRequestError('Los datos proporcionados como parámetros no son válidos para la solicitud.'))
  }

  const { character, user } = req.body
  if (!character || !user) {
    return next(new HTTPErrors.BadRequestError('Los datos proporcionados como parámetros no son válidos para la solicitud.'))
  }

  let result
  try {
    const { _id } = await createCharacterClickAudit({
      userName: user.name,
      userMail: user.email,
      characterName: character.name,
      characterUrl: character.url
    })
    result = {
      code: '200',
      message: `Se creo el registro con id: ${_id}`
    }
  } catch (error) {
    return next(error)
  }

  res
    .status(200)
    .send(result)
}

module.exports = {
  getCharacters,
  auditClickCharacters
}
