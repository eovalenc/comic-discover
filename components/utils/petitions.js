'use strict'

const axios = require('axios')
const { marvelApi: { protocol, host, uri, keys: { public: pub, private: pri } } } = require('../../config')

// Ruta de la api
const path = `${protocol}${host}${uri}`
const hash = require('crypto').createHash('md5').update(`${1}${pri}${pub}`).digest('hex')
// Headers de las peticiones
const headers = {
  'Content-Type': 'application/json'
}

// Rutas para peticiones
const get = (route, query) => (
  axios.get(`${path}/${route}?ts=1&apikey=${pub}&hash=${hash}${query}`, {
    headers
  })
)
const find = (route, id) => (
  axios.get(`${path}/${route}/${id}`, {
    headers
  })
)

const post = (route, data) => (
  axios.post(`${path}/${route}`, data, {
    headers
  })
)

module.exports = {
  get,
  post,
  find
}
