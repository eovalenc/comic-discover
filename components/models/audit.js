'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AuditSchema = new Schema({
  userName: { type: String, required: true },
  userMail: { type: String, required: true },
  characterName: { type: String, required: true },
  characterUrl: { type: String, required: true }
})

module.exports = mongoose.model('Audit', AuditSchema)
