'use strict'

const express = require('express')
const asyncify = require('express-asyncify')

const { marvel } = require('./marvel')

const router = asyncify(express.Router())

router.use((req, res, next) => {
  // Website you wish to allow to connect
  res.header('Access-Control-Allow-Origin', '*')
  // Request headers you wish to allow
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method')
  // Request methods you wish to allow
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
  res.header('Allow', 'GET, POST')
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true)
  res.setHeader('Content-Type', 'application/json')
  next()
})

router.use(marvel)

module.exports = router
