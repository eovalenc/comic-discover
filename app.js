'use strict'

const express = require('express')
const debug = require('debug')('comic-discover:app')
const winston = require('./config/winston')
const morgan = require('morgan')
const chalk = require('chalk')
const asyncify = require('express-asyncify')
const bodyParser = require('body-parser')
const compression = require('compression')
const helmet = require('helmet')
const HTTPErrors = require('http-custom-errors')

// Cargar rutas
const components = require('./components')
const app = asyncify(express())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(compression()) // Compress all routes
app.use(helmet())
app.use(helmet.noCache())
app.use(morgan('combined', {
  stream: winston.stream
}))

// app.use(`/${context}`, express.static(path.join(__dirname, `/${source}`)))
app.use('/v1', components)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  if (!req.route) {
    const HTTPErrors = require('http-custom-errors')
    throw new HTTPErrors.NotFoundError('Resource not found')
  }
  next()
})

// Error handler
app.use(function (err, req, res, next) {
  try {
    // set locals, only providing error in development
    console.error(err)
    console.error(err.stack)
    debug('Error %O:', err)
    debug('Error %O:', err.message)

    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    // add this line to include winston logging
    winston.error(`${err.code || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`)
  } catch (error) {
    console.log(error)
    err = new HTTPErrors.InternalServerError('Se produjo un error interno desconocido.')
  }
  delete err.stack
  res.status(err.code || 500)
  res.send(err)
})

function handleFatalError (err, promise) {
  console.error(`${chalk.red('[fatal error]')} ${err.message}`)
  console.error(err.stack)
  console.error('Unhandled rejection (promise: ', promise, ', reason: ', err, ').')
  process.exit(1)
}

process.on('uncaughtException', handleFatalError)
process.on('unhandledRejection', handleFatalError)

module.exports = app
