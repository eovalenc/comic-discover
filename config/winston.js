'use strict'

const appRoot = require('app-root-path')
const winston = require('winston')

// define the custom settings for each transport (file, console)
const options = {
  file: {
    level: 'info',
    name: 'info-file',
    filename: `${appRoot}/logs/filelog-info.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false
  },
  fileError: {
    level: 'error',
    name: 'error-file',
    filename: `${appRoot}/logs/filelog-error.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5,
    colorize: false
  },
  console: {
    level: 'warn',
    handleExceptions: true,
    json: false,
    colorize: true
  }
}

// instantiate a new Winston Logger with the settings defined above
const logger = new winston.Logger({
  transports: [
    new winston.transports.File(options.file),
    new winston.transports.File(options.fileError),
    new winston.transports.Console(options.console)
  ],
  exceptionHandlers: [
    new winston.transports.File({ filename: `${appRoot}/logs/exceptions.log` })
  ],
  exitOnError: false // do not exit on handled exceptions
})

// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
  write: function (message, encoding) {
    // use the 'info' log level so the output will be picked up by both transports (file and console)
    logger.info(message)
  }
}

module.exports = logger
