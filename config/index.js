'use strict'

module.exports = {
  port: process.env.PORT || '3000',
  source: process.env.SOURCE || 'src',
  db: {
    jdbc: process.env.DATABASE_URL || 'mongodb://localhost:27017/audit'
  },
  auth: {
    secret: process.env.SECRET || 'innova'
  },
  marvelApi: {
    protocol: process.env.MARVEL_PROTOCOL || 'https://',
    host: process.env.MARVEL_HOST || 'gateway.marvel.com/',
    uri: process.env.MARVEL_API || 'v1/public/',
    keys: {
      public: process.env.MARVEL_PUBLIC_KEY || 'a376fcfa63035a856b49a7ba0b85f8cd',
      private: process.env.MARVEL_PRIVATE_KEY || '9a0e58e7f53bd6fdba453e40fcf71864ea981277'
    }
  }
}
