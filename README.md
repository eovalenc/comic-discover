# comic-discover

Prueba técnica Consultor Fullstack. Siguiendo el archivo PDF de la prueba se utilizo para el desarrollo.

  - Angular V6
  - Bulma CSS
  - ExpressJS
  - NodeJs
  - MongoDB
 
## Installation

comic-discover requiere [Node.js](https://nodejs.org/) v8+ para correr.

Instalar las dependencias del proyecto y arrancar el servidor.

Para SO basados en UNIX ejecutar:
```sh
$ cd comic-discover
$ npm install -d
$ npm run dev
```

Para SO Windows
```sh
$ cd comic-discover
$ npm install -d
$ npm run dev:win
```

License
----

MIT


**Edwin Valencia, [https://www.linkedin.com/in/edwinvalencia/]**
